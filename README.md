#### Build commands

* `npm run start` ─ compile assets when file changes are made, start [webpack-dev-server](https://github.com/webpack/webpack-dev-server) session
* `npm run build` ─ compile and optimize (the files in your assets directory) for production
=======


