export const SearchPopUpClose = (() => {
  let btn = [document.querySelector(".header__search"), document.querySelector(".search__close")];
  let search = document.querySelector(".search");
  let input = document.querySelector(".search__input input");
  btn.forEach(element => {
    element.onclick = function () {
      btn[0].classList.remove("active")
      search.classList.remove("active");
      !search.classList.contains("active") && (input.value = "")
    }
  })
})()
