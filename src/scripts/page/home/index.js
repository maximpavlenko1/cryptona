import { FeaturedSectionSlider, } from "./components/featuredSlider";
import { TopSectionSlider } from "./components/topSlider";

export const Home = (() => {
  FeaturedSectionSlider();
  TopSectionSlider();
})()

