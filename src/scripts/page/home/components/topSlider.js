import Swiper from "swiper";

export const TopSectionSlider = () => {
  new Swiper('.top__slider', {
    slidesPerView: "auto",
    spaceBetween: 16,
    breakpoints: {
      1200: {
        slidesPerView: 6,
        noSwiping: false,
        allowSlidePrev: false,
        allowSlideNext: false
      },
    }
  })
}
