import Swiper from "swiper";

export const FeaturedSectionSlider = () => {
  new Swiper('.featured__slider', {
    slidesPerView: "auto",
    spaceBetween: 16,
    loop: true,
    slideToClickedSlide:true,
    breakpoints: {
      1200: {
        slidesPerView: 5,
        spaceBetween: 16,
        centeredSlides: true,
      },
    }
  })
}
