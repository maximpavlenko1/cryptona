import { RemoveActivePopUp } from "@helpers/removeActivePopUp";

export const ActivePopup = (() => {
  let popupBtn = document.querySelectorAll("[data-target]");
  popupBtn.forEach(element => {
    element.onclick = function () {
      let popup = document.querySelector(`.${element.dataset.target}`);
      if (!element.classList.contains("active")) {
        RemoveActivePopUp(popupBtn);
        element.classList.add("active");
        popup.classList.add("active");
      } else {
        element.classList.remove("active");
        popup.classList.remove("active");
      }
    }
  })

})()
