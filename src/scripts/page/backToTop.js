export const BackToTop = (() => {
  function clickLinkTop() {
    if (window.pageYOffset > 0) {
      window.scrollBy(0, -80);
      setTimeout(clickLinkTop, 0);
    }
  }

  let goTopBtn = document.querySelector('.back__top');
  goTopBtn.addEventListener('click', clickLinkTop);
})()
